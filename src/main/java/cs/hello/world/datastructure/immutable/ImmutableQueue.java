package cs.hello.world.datastructure.immutable;
import java.util.NoSuchElementException;
/**
 * 
 * @author Carl Adler(C.A.)
 *
 * The Queue class represents an immutable first-in-first-out (FIFO) queue of objects.
 * @param <E>
 */
public class ImmutableQueue<E> {
	/**
	 * The stackForEnqueue is for enqueue, and the stackForDequeue is for dequeue.
	 */
	private ImmutableStack<E> stackForEnqueue;
	private ImmutableStack<E> stackForDequeue;
	
	/**
	 * requires default constructor.
	 */
	@SuppressWarnings("unchecked")
	public ImmutableQueue() {
		// modify this constructor if necessary, but do not remove default
		// constructor
		this.stackForEnqueue = ImmutableStack.obtainEmptyStack();
		this.stackForDequeue = ImmutableStack.obtainEmptyStack();
	}
	
	public ImmutableQueue(ImmutableStack<E> stackForEnqueue, ImmutableStack<E> stackForDequeue) {
		this.stackForEnqueue = stackForEnqueue;
		this.stackForDequeue = stackForDequeue;
	}

	// add other constructors if necessary
	/**
	 * Returns the queue that adds an item into the tail of this queue without
	 * modifying this queue.
	 * 
	 * <pre>
	 * e.g.
	 * When this queue represents the queue (2, 1, 2, 2, 6) and we enqueue the value 4 into this queue,
	 * this method returns a new queue (2, 1, 2, 2, 6, 4)
	 * and this object still represents the queue (2, 1, 2, 2, 6) .
	 * </pre>
	 * 
	 * If the element e is null, throws IllegalArgumentException.
	 * 
	 * @param e
	 * @return
	 * @throws IllegalArgumentException
	 */
	public ImmutableQueue<E> enqueue(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		return new ImmutableQueue<E>(this.stackForEnqueue.push(e), this.stackForDequeue);
	}

	/**
	 * Returns the queue that removes the object at the head of this queue
	 * without modifying this queue.
	 * 
	 * <pre>
	 * e.g.
	 * When this queue represents the queue (7, 1, 3, 3, 5, 1) ,
	 * this method returns a new queue (1, 3, 3, 5, 1)
	 * and this object still represents the queue (7, 1, 3, 3, 5, 1) .
	 * </pre>
	 * 
	 * If this queue is empty, throws java.util.NoSuchElementException.
	 * 
	 * @return
	 * @throws java.util.NoSuchElementException
	 */
	@SuppressWarnings("unchecked")
	public ImmutableQueue<E> dequeue() {
		if(this.isEmpty()) {
			throw new NoSuchElementException();
		} else if(!this.stackForDequeue.isEmpty()) {
			return new ImmutableQueue<E>(this.stackForEnqueue, this.stackForDequeue.getBody());
		} else {
			return new ImmutableQueue<E>(ImmutableStack.obtainEmptyStack(), this.stackForEnqueue.obtainReversedStack().getBody());
		}
	}

	/**
	 * Looks at the object which is the head of this queue without removing it
	 * from the queue.
	 * 
	 * <pre>
	 * e.g.
	 * When this queue represents the queue (7, 1, 3, 3, 5, 1),
	 * this method returns 7 and this object still represents the queue (7, 1, 3, 3, 5, 1)
	 * </pre>
	 * 
	 * If the queue is empty, throws java.util.NoSuchElementException.
	 * 
	 * @return
	 * @throws java.util.NoSuchElementException
	 */
	public E peek() {
		if(this.isEmpty()) {
			throw new NoSuchElementException();
		} else if(this.stackForDequeue.isEmpty()) {
			adjustQueueContent();
		}
		return this.stackForDequeue.getHead();
	}
	
	/**
	 * Returns the number of objects in this queue.
	 * 
	 * @return
	 */
	public int size() {
		return this.stackForEnqueue.getSize() + this.stackForDequeue.getSize();
	}
	
	/**
	 * Check the queue is empty or not.
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * When calling the peek() method, if the stackForDequeue
	 * is empty, then we need to reverse the stackForEnqueue
	 * and get the head of it for return value. It looks like 
	 * a very simple operation, but when the frequency of peek 
	 * increased, we need to reverse the stackForEnqueue for 
	 * several times, this might slow down the entire performance.
	 * To solve this problem, I decided to adjust the content
	 * of the two stacks to prevent such kind of performance issue.
	 */
	@SuppressWarnings("unchecked")
	private void adjustQueueContent() {
		this.stackForDequeue = this.stackForEnqueue.obtainReversedStack();
		this.stackForEnqueue = ImmutableStack.obtainEmptyStack();
	}
	
	/**
	 * Append a queue to the other queue.
	 * 
	 * @param queue
	 * @return
	 */
	public ImmutableQueue<E> append(ImmutableQueue<E> queue) {
		if(queue == null) {
			throw new IllegalArgumentException();
		} else if(queue.isEmpty()) {
			return this;
		}
		ImmutableQueue<E> temp = queue;
		ImmutableQueue<E> result = this;
		for(int i = 0; i < queue.size(); i++) {
			result = result.enqueue(temp.peek());
			temp = temp.dequeue();
		}
		return result;
	}
	
	public String toString() {
		if(isEmpty()) {
			return "";
		} else if(this.stackForDequeue.isEmpty()) {
			return this.stackForEnqueue.obtainReversedStack().toString();
		} else if(this.stackForEnqueue.isEmpty()) {
			return this.stackForDequeue.toString();
		} else {
			return stackForDequeue.toString() + stackForEnqueue.obtainReversedStack().toString();
		}
	}
}