package cs.hello.world.datastructure.immutable;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class ImmutableStack<E> {
	
	private E head;
	private ImmutableStack<E> body;
	private int size;
	
	public ImmutableStack() {
		this.head = null;
		this.body = null;
		this.size = 0;
	}
	
	public ImmutableStack(E head, ImmutableStack<E> body) {
		this.head = head;
		this.body = body;
		this.size = body.size + 1;
	}
	
	@SuppressWarnings("rawtypes")
	public static ImmutableStack obtainEmptyStack() {
		return new ImmutableStack();
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public E getHead() {
		return head;
	}

	public ImmutableStack<E> getBody() {
		return body;
	}

	public int getSize() {
		return size;
	} 
	
	public ImmutableStack<E> push(E obj) {
		return new ImmutableStack<E>(obj, this);
	}

	public ImmutableStack<E> obtainReversedStack() {
		ImmutableStack<E> reversedStack = new ImmutableStack<E>();
		ImmutableStack<E> currentStack = this;
		while(!currentStack.isEmpty()) {
			reversedStack = reversedStack.push(currentStack.head);
			currentStack = currentStack.body;
		}
		return reversedStack;
	}
	
	public String toString() {
		String stackContent = this.head.toString();
		ImmutableStack<E> bodyForPrint = this.body;
		while(!bodyForPrint.isEmpty()) {
			stackContent = stackContent.concat("," + bodyForPrint.head.toString());
			bodyForPrint = bodyForPrint.body;
		}
		return stackContent;
	}
	
}
