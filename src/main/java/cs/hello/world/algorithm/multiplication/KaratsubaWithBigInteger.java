package cs.hello.world.algorithm.multiplication;

import java.math.BigInteger;

/**
 * 
 * @author Carl Adler (C.A.)
 *
 */
public class KaratsubaWithBigInteger {
	
	public static BigInteger multiplyByKaratsuba(BigInteger x, BigInteger y) {
		int N = Math.max(x.bitLength(), y.bitLength());
		if(N <= 1000)
			return x.multiply(y);
		
		N = (N / 2) + (N % 2);
		
		// a
		BigInteger high1 = x.shiftRight(N);
		// b
		BigInteger low1 = x.subtract(high1.shiftLeft(N));
		// c
		BigInteger high2 = y.shiftRight(N);
		// d
		BigInteger low2 = y.subtract(high2.shiftLeft(N));
		
		// ac
		BigInteger p1 = multiplyByKaratsuba(high1, high2);
		// bd
		BigInteger p2 = multiplyByKaratsuba(low1, low2);
		// (a+b) * (c+d)
		BigInteger p3 = multiplyByKaratsuba(high1.add(low1), high2.add(low2));
		
		return p2.add(p3.subtract(p1).subtract(p2).shiftLeft(N)).add(p1.shiftLeft(2*N));
	}

}
