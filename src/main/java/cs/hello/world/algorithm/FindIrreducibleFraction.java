package cs.hello.world.algorithm;

import java.util.ArrayList;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class FindIrreducibleFraction {
	
	private int a;
	private int b;
	private ArrayList<String> irreducible = null;
	private ArrayList<String> reducible = null;
	
	private int findCommonDivisor(int a, int b) {
		int r;
		int n = a;
		int m = b;
		r = n % m;
		while(r != 0) {
			n = m;
			m = r;
			r = n % m;
		}
		return m;
	}
	
	public void setNum(int n, int m) {
		if (n < m) {
			throw new IllegalArgumentException();
		}
		a = n;
		b = m;
	}
	
	public void findFractions() {
		irreducible = new ArrayList<String>();
		reducible = new ArrayList<String>();
		
		for(int p = b; p <= a; p++) {
			for(int q = 1; q < b; q++) {
				if(this.findCommonDivisor(p, q) == 1) {
					irreducible.add(q + "/" + p);
				} else {
					reducible.add(q + "/" + p);
				}
			}
		}
	}
	
	public void showResults() {
		System.out.append("Irreducible: ");
		for (String result : irreducible) {
			System.out.append(result + " ");
		}
		System.out.append("\n");
		System.out.append("Reducible: ");
		for (String result : reducible) {
			System.out.append(result + " ");
		};
	}
	
	public ArrayList<String> getIrreducible() {
		return irreducible;
	}

	public ArrayList<String> getReducible() {
		return reducible;
	}

	public static void main(String[] args) {
		FindIrreducibleFraction findIrreducibleFraction = new FindIrreducibleFraction();
		findIrreducibleFraction.setNum(5, 3);
		findIrreducibleFraction.findFractions();
		findIrreducibleFraction.showResults();
	}

}
