package cs.hello.world.algorithm;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class Permutation {
	
	private char[] data;
	private int length;
	
	public void swap(char[] data, int positionA, int positionB) {
		char temp = data[positionA];
		data[positionA] = data[positionB];
		data[positionB] = temp;
	}
	
	public void reverse(char[] data, int positionA, int positionB) {
		for(; positionA < positionB; positionA++, positionB--) {
			swap(data, positionA, positionB);
		}
	}
	
	public boolean findNextPermutation() {
		int start = length - 1;
		int end = length - 1;
		while(start > 0 && data[start] <= data[start - 1]) {
			start--;
		}
		if(start == 0) {
			return false;
		}
		while(data[start - 1] >= data[end]) {
			end--;
		}
		if(start - 1 != end) {
			swap(data, start - 1, end);
		}
		reverse(data, start, length - 1);
		return true;
	}
	
	public void showPermutationSet(String input) {
		length = input.length();
		data = new char[length];
		for(int i = 0; i < length; i++) {
			data[i] = input.charAt(i);
		}
		for(int i = 0; i < length; i++) {
			int min = i;
			for(int j = i + 1; j < length; j++) {
				if(data[min] > data[j]) {
					min = j;
				}
			}
			if(min != i) {
				swap(data, min, i);
			}
		}
		System.out.println(data);
		while(findNextPermutation()) {
			System.out.println(data);
		}
	}
	
	public static void main(String[] args) {
		Permutation permutation = new Permutation();
		permutation.showPermutationSet("dcba");
	}
	
}
