package cs.hello.world.algorithm.sorting;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MergeSort {

	private int[] data;
	private int[] tempArrayForMerge;
	private int length;
	
	public void sort(int[] input) {
		length = input.length;
		data = input;
		tempArrayForMerge = new int[length];
		doMergeSort(data, 0, length - 1);
	}
	
	private void doMergeSort(int[] input, int leftIndex, int rightIndex) {
		if(leftIndex < rightIndex) {
			int middle = leftIndex + ((rightIndex - leftIndex) / 2);
			doMergeSort(input, leftIndex, middle);
			doMergeSort(input, middle + 1, rightIndex);
			merge(input, leftIndex, middle + 1, rightIndex);
		}
	}

	private void merge(int[] inputData, int leftIndex, int rightIndex, int endIndex) {
		int leftPosition = leftIndex;
		int pointerForTemp = leftIndex;
		int leftBound = rightIndex - 1;

		while(leftPosition <= leftBound && rightIndex <= endIndex)
			if(inputData[leftPosition] < inputData[rightIndex])
				tempArrayForMerge[pointerForTemp++] = inputData[leftPosition++];
			else
				tempArrayForMerge[pointerForTemp++] = inputData[rightIndex++];
		
		while (leftPosition <= leftBound) 
			tempArrayForMerge[pointerForTemp++] = inputData[leftPosition++];
		
		while (rightIndex <= endIndex)
			tempArrayForMerge[pointerForTemp++] = inputData[rightIndex++];

		for(int i = leftIndex; i <= endIndex ; i ++)
			inputData[i] = tempArrayForMerge[i];
	}

	public int[] getData() {
		return data;
	}
	
}
