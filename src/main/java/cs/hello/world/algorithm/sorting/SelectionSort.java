package cs.hello.world.algorithm.sorting;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class SelectionSort {
	
	public static int[] sort(int[] input) {
		for(int i = 0; i < input.length; i++) {
			int min = i;
			for(int j = i + 1; j < input.length; j++) {
				if(input[min] > input[j])
					min = j;
			}
			if(min != i)
				swap(input, min, i);
		}
		return input;
	}
	
	private static void swap(int[] input, int positionA, int positionB) {
		int temp = input[positionA];
		input[positionA] = input[positionB];
		input[positionB] = temp;
	}
	
}
