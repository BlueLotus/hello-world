package cs.hello.world.algorithm.sorting;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class InversionCounter {
	
	private int[] data;
	private int[] buffer;
	private int length;
	
	public long countInversions(int[] input) {
		length = input.length;
		data = input;
		buffer = new int[length];
		return count(data, 0, length - 1);
	}
	
	private long count(int[] data, int left, int right) {
		if(left >= right)
			return 0L;
		
		int mid = left + ((right - left) / 2);
		
		return count(data, left, mid) + count(data, mid + 1, right) + merge(data, left, mid + 1, right);
	}
	
	private long merge(int[] data, int left, int mid, int right) {
		long inversion = 0L;
		
		int leftPosition = left;
		int pointerForBuff = left;
		int leftBound = mid - 1;
		int rightPosition = mid;
		
		while(leftPosition <= leftBound && rightPosition <= right)
			/*
			 * 
			 * Here I use "data[leftPosition] <= data[rightPosition]"
			 * as the condition to prevent zero inversion case.
			 * 
			 * e.g. data = (1, 1, 1, 1), the inversion should be 0,
			 * but if the condition is lack of "=",
			 * it will see (1, 1) as an inversion.
			 * 
			 */
			if(data[leftPosition] <= data[rightPosition])
				buffer[pointerForBuff++] = data[leftPosition++];
			else {
				buffer[pointerForBuff++] = data[rightPosition++];
				inversion += (mid - leftPosition);
			}
		
		while(leftPosition <= leftBound)
			buffer[pointerForBuff++] = data[leftPosition++];
		
		while(rightPosition <= right)
			buffer[pointerForBuff++] = data[rightPosition++];
		
		for(int i = left; i <= right; i++)
			data[i] = buffer[i];
		
		return inversion;
	}

}
