package cs.hello.world.algorithm.sorting;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class InsertionSort {
	
	public static int[] sort(int[] input) {
		for(int i = 1; i < input.length; i++) {
			for(int j = i; j > 0; j--) {
				if(input[j] < input[j - 1])
					swap(input, j, j - 1);
			}
		}
		return input;
	}
	
	private static void swap(int[] input, int positionA, int positionB) {
		int temp = input[positionA];
		input[positionA] = input[positionB];
		input[positionB] = temp;
	}

}
