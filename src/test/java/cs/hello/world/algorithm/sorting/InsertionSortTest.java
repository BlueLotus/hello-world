package cs.hello.world.algorithm.sorting;

import static org.junit.Assert.*;

import org.junit.Test;

public class InsertionSortTest {

	@Test
	public void test() {
		String expectedResult = "0 2 23 35 48 79 85 100 245 584 ";
		int[] input = new int[]{100,584,23,79,35,85,2,48,0,245};
		int[] result = InsertionSort.sort(input);
		assertEquals(expectedResult, getResult(result));
	}
	
	public String getResult(int[] input) {
		String result = "";
		for (int i : input) {
			result = result.concat(i + " ");
		}
		return result;
	}

}
