package cs.hello.world.algorithm.sorting;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MergeSortingTest {
	
	public static MergeSort mergeSort;
	public static int[] unsortedData;
	public static int[] sortedData;
	public static int[] expectedData;

	@BeforeClass
	public static void init() {
		initTestData();
	}
	
	@Test
	public void testAccuracyForMergeSort() {
		expectedData = Arrays.copyOf(unsortedData, unsortedData.length);
		Arrays.sort(expectedData);
		mergeSort.sort(unsortedData);
		assertEquals(getResultString(expectedData), getResultString(mergeSort.getData()));
	}
	
	public static void initTestData() {
		mergeSort = new MergeSort();
		
		Random random = new Random();
		unsortedData = new int[random.nextInt(10)];
		for(int i = 0; i < unsortedData.length; i++) {
			unsortedData[i] = random.nextInt(100);
		}
	}
	
	public static String getResultString(int[] data) {
		String result = "";
		for (int i : data) {
			result = result.concat(String.valueOf(i));
		}
		return result;
	}

}
