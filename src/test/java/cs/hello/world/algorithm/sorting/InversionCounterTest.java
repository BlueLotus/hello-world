package cs.hello.world.algorithm.sorting;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class InversionCounterTest {
	
	private static InversionCounter inversionCounter;
	
	@BeforeClass
	public static void init() {
		inversionCounter = new InversionCounter();
	}

	@Test
	public void testForOneInversion() {
		int[] a1 = { 2, 4, 1, 3, 5 };
		assertEquals(3, inversionCounter.countInversions(a1));
	}

	@Test
	public void testForTwoInversions() {
		int[] a2 = { 4, 3, 2, 1 };
		assertEquals(6, inversionCounter.countInversions(a2));
	}

	@Test
	public void testForThreeInversions() {
		int[] a3 = { 1, 2, 3, 4 };
		assertEquals(0, inversionCounter.countInversions(a3));
	}

	@Test
	public void testForFourInversions() {
		int[] a3 = { 3, 3, 3, 3 };
		assertEquals(0, inversionCounter.countInversions(a3));
	}
	
	@Test
	public void testForRandomHugeInput() {
		int[] dataInv = new int[100000];
        Random rand = new Random();
        
        for (int i = 0; i < dataInv.length; i++) {
            dataInv[i] = rand.nextInt();
        }
        
        System.out.println("Input size: " + dataInv.length);
        System.out.println("Inversions:" + inversionCounter.countInversions(dataInv));
	}
	
}
