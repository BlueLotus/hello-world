package cs.hello.world.algorithm.multiplication;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Test;

public class KaratsubaWithBigIntegerTest {
	
	@Test
	public void testForResultIntegrity() {
		
		Random random = new Random();
		int N  = 2000;
		BigInteger x = new BigInteger(N, random);
		BigInteger y = new BigInteger(N, random);
		
		BigInteger result = KaratsubaWithBigInteger.multiplyByKaratsuba(x, y);
		
		BigInteger resultByJDK = x.multiply(y);
		
		assertEquals(resultByJDK, result);
		
	}

}
