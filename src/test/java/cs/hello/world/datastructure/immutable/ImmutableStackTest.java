package cs.hello.world.datastructure.immutable;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import cs.hello.world.datastructure.immutable.ImmutableStack;

/**
 * 
 * @author Carl Alder(C.A.)
 *
 */
public class ImmutableStackTest {
	
	private static final String VALUE_FOR_NORMAL_STACK = "5,4,3,2,1";
	private static final String VALUE_FOR_REVERSED_STACK = "1,2,3,4,5";
	
	private static ImmutableStack<Integer> testStack;
	private static ImmutableStack<Integer> normalStack;
	private static ImmutableStack<Integer> reversedStack;
	
	@BeforeClass
	public static void init() {
		normalStack = new ImmutableStack<Integer>();
		normalStack = normalStack.push(1);
		normalStack = normalStack.push(2);
		normalStack = normalStack.push(3);
		normalStack = normalStack.push(4);
		normalStack = normalStack.push(5);
	}

	@After
	public void tearDown() {
		testStack = null;
	}
	
	@Test
	public void testForIsEmpty() {
		testStack = new ImmutableStack<Integer>();
		assertTrue(testStack.isEmpty());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testForObtainEmptyStack() {
		testStack = ImmutableStack.obtainEmptyStack();
		assertEquals(0, testStack.getSize());
	}
	
	@Test
	public void testForPush() {
		testStack = new ImmutableStack<Integer>();
		int count = 100;
		for(int i = 1; i <= count; i++) {
			testStack = testStack.push(i);
		}
		assertEquals(count, testStack.getSize());
	}
	
	@Test
	public void testForGetSize() {
		testStack = new ImmutableStack<Integer>();
		testStack = testStack.push(100);
		assertTrue(testStack.getSize() == 1);
	}
	
	@Test
	public void testForObtainReversedStack() {
		String expectedValueForReversedStack = VALUE_FOR_REVERSED_STACK;
		reversedStack = normalStack.obtainReversedStack();
		assertEquals(expectedValueForReversedStack, reversedStack.toString());
	}
	
	@Test
	public void testForToString() {
		String expectedValueForNormalStack = VALUE_FOR_NORMAL_STACK;
		assertEquals(expectedValueForNormalStack, normalStack.toString());
	}

}
