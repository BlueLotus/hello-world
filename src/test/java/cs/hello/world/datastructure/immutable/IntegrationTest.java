package cs.hello.world.datastructure.immutable;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ ImmutableQueueTest.class, ImmutableStackTest.class })
public class IntegrationTest {

}
