package cs.hello.world.datastructure.immutable;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import cs.hello.world.datastructure.immutable.ImmutableQueue;
import cs.hello.world.datastructure.immutable.ImmutableStack;
import cs.hello.world.util.Order;
import cs.hello.world.util.OrderedRunner;

/**
 * 
 * @author Carl Alder(C.A.)
 *
 */
@RunWith(OrderedRunner.class)
public class ImmutableQueueTest {
	
	private static final String STRING_VALUE_FOR_QUEUE1 = "2,1,2,2,6";
	private static final String STRING_VALUE_FOR_QUEUE2 = "7,1,3,3,5,1";
	private static final int TEST_COUNT = 1000000;
	
	private static ImmutableStack<Integer> testStack1;
	private static ImmutableStack<Integer> testStack2;
	private static ImmutableStack<Integer> emptyStack;
	private static ImmutableQueue<Integer> queue;
	private static String expectedValue;
	private static int expectedQueueSize;
	
	@BeforeClass
	public static void init() {
		/* Create a stack to represent (2,1,2,2,6) in a queue */
		testStack1 = new ImmutableStack<Integer>();
		testStack1 = testStack1.push(2);
		testStack1 = testStack1.push(1);
		testStack1 = testStack1.push(2);
		testStack1 = testStack1.push(2);
		testStack1 = testStack1.push(6);
		
		/* Create a stack to represent (7,1,3,3,5,1) in a queue */
		testStack2 = new ImmutableStack<Integer>();
		testStack2 = testStack2.push(7);
		testStack2 = testStack2.push(1);
		testStack2 = testStack2.push(3);
		testStack2 = testStack2.push(3);
		testStack2 = testStack2.push(5);
		testStack2 = testStack2.push(1);
		
		emptyStack =  new ImmutableStack<Integer>();
		
		expectedQueueSize = 0;
	}
	
	@After
	public void tearDown() {
		expectedValue = null;
		expectedQueueSize = 0;
		queue = null;
	}
	
	@Order(order = 1)
	@Test
	public void testForIsEmpty() {
		queue = new ImmutableQueue<Integer>();
		assertTrue(queue.isEmpty());
	}
	
	@Order(order = 2)
	@Test
	public void testForSize() {
		queue = new ImmutableQueue<Integer>();
		assertEquals(0, queue.size());
		queue = queue.enqueue(100);
		assertEquals(1, queue.size());
		queue = queue.enqueue(200);
		assertEquals(2, queue.size());
		queue = queue.dequeue();
		assertEquals(1, queue.size());
	}
	
	@Order(order = 3)
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionForEnqueue() {
		queue = new ImmutableQueue<Integer>();
		Integer integer = null;
		queue.enqueue(integer);
	}
	
	@Order(order = 4)
	@Test
	public void testForEnqueue() {
		int newValue = 5;
		expectedQueueSize = testStack1.getSize() + 1;
		expectedValue = STRING_VALUE_FOR_QUEUE1 + "," + newValue;
		
		queue = new ImmutableQueue<Integer>(testStack1, emptyStack);
		queue = queue.enqueue(newValue);
		
		assertEquals(expectedValue, queue.toString());
		assertEquals(expectedQueueSize, queue.size());
	}
	
	@Order(order = 5)
	@Test(expected = NoSuchElementException.class)
	public void testNoSuchElementExceptionForDequeue() {
		queue = new ImmutableQueue<Integer>();
		queue.dequeue();
	}
	
	@Order(order = 6)
	@Test
	public void testForDequeue() {
		expectedQueueSize = testStack2.getSize() - 1;
		expectedValue = STRING_VALUE_FOR_QUEUE2.substring(2);
		
		queue = new ImmutableQueue<Integer>(testStack2, emptyStack);
		queue = queue.dequeue();

		assertEquals(expectedValue, queue.toString());
		assertEquals(expectedQueueSize, queue.size());
	}
	
	@Order(order = 7)
	@Test(expected = NoSuchElementException.class)
	public void testNoSuchElementExceptionForPeek() {
		queue = new ImmutableQueue<Integer>();
		queue.peek();
	}
	
	@Order(order = 8)
	@Test
	public void testForPeek() {
		expectedQueueSize = testStack2.getSize();
		expectedValue = STRING_VALUE_FOR_QUEUE2.substring(0, 1);
		
		queue = new ImmutableQueue<Integer>(testStack2, emptyStack);
		
		assertEquals(expectedValue, String.valueOf(queue.peek()));
		assertEquals(expectedQueueSize, queue.size());
	}
	
	@Order(order = 9)
	@Test(expected = IllegalArgumentException.class)
	public void testForAppendFailed() {
		ImmutableQueue<Integer> nullQueue = null;
		queue = new ImmutableQueue<Integer>(testStack1, emptyStack);
		
		queue.append(nullQueue);
	}
	
	@Order(order = 10)
	@Test
	public void testForAppend() {
		ImmutableQueue<Integer> queueForAppend = new ImmutableQueue<Integer>(testStack2, emptyStack);
		ImmutableQueue<Integer> emptyQueue = new ImmutableQueue<Integer>();
		queue = new ImmutableQueue<Integer>(testStack1, emptyStack);
		
		expectedQueueSize = testStack1.getSize();
		expectedValue = STRING_VALUE_FOR_QUEUE1;
		queue = queue.append(emptyQueue);
		assertEquals(expectedValue, queue.toString());
		assertEquals(expectedQueueSize, queue.size());
		
		expectedQueueSize = testStack1.getSize() + testStack2.getSize();
		expectedValue = STRING_VALUE_FOR_QUEUE1 + "," + STRING_VALUE_FOR_QUEUE2;
		queue = queue.append(queueForAppend);
		assertEquals(expectedValue, queue.toString());
		assertEquals(expectedQueueSize, queue.size());
	}
	
	@Order(order = 11)
	@Test
	public void testForGeneralUsage() {
		queue = new ImmutableQueue<Integer>();
		assertTrue(queue.isEmpty());
		queue = queue.enqueue(100);
		assertEquals(new Integer(100), queue.peek());
		assertEquals(1, queue.size());
		queue = queue.enqueue(25);
		queue = queue.enqueue(68);
		queue = queue.enqueue(43);
		queue = queue.enqueue(68548);
		assertEquals(5, queue.size());
		assertEquals(new Integer(100), queue.peek());
		assertEquals("25,68,43,68548", queue.dequeue().toString());
	}
	
	/**
	 * Test 10~13 are all performance tests, and the test 
	 * failed thresholds are according to the environment 
	 * of my test laptop. The performance might be changed
	 * in different situations. For example, test No.13
	 * can be finished in about 0.06s in Eclipse IDE (run 
	 * JUnit test case directly), but if the case was run 
	 * with maven, it might cost over 0.12s.
	 */
	@Order(order = 12)
	@Test
	public void testForEnqueuePerformanceWithForLoop() {
		long start = System.currentTimeMillis();
		queue = new ImmutableQueue<Integer>();
		
		for(int i = 0; i < TEST_COUNT; i++) {
			queue = queue.enqueue((int) (Math.random() * 10));
		}
		
		long end = System.currentTimeMillis();
		long totalTime = end - start;
		assertTrue(totalTime < 300);
	}
	
	@Order(order = 13)
	@Test
	public void testForEnqueueAndDequeuePerformanceWithForLoop() {
		long start = System.currentTimeMillis();
		queue = new ImmutableQueue<Integer>();
		
		for(int i = 0; i < TEST_COUNT; i++) {
			queue = queue.enqueue((int) (Math.random() * 10));
		}
		
		for(int i = 0; i < TEST_COUNT; i++) {
			queue = queue.dequeue();
		}
		
		long end = System.currentTimeMillis();
		long totalTime = end - start;
		assertTrue(totalTime < 400);
	}
	
	@Order(order = 14)
	@Test
	public void testForEnqueuePerformanceWithWhileLoop() {
		long start = System.currentTimeMillis();
		queue = new ImmutableQueue<Integer>();
		int count = 0;
		
		while(++count < TEST_COUNT){
			queue = queue.enqueue((int) (Math.random() * 10));
		}
		
		long end = System.currentTimeMillis();
		long totalTime = end - start;
		assertTrue(totalTime < 200);
	}
	
	@Order(order = 15)
	@Test
	public void testForEnqueueAndDequeuePerformanceWithWhileLoop() {
		long start = System.currentTimeMillis();
		queue = new ImmutableQueue<Integer>();
		int count = 0;
		
		while(++count < TEST_COUNT) {
			queue = queue.enqueue((int) (Math.random() * 10));
		}
		
		while(--count > 0) {
			queue = queue.dequeue();
		}
		
		long end = System.currentTimeMillis();
		long totalTime = end - start;
		assertTrue(totalTime < 200);
	}

}
