package cs.hello.world.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Carl Adler (C.A.)
 * */
@Retention(RetentionPolicy.RUNTIME)
public @interface Order {
    public int order();
}
